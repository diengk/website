


jQuery(document).ready(function($) {
	/**
	 *
	 * Preloading Page
	 *
	 */

	var initialLoad = true;
	if(initialLoad){
		setTimeout(function() {
			jQuery('#loading').fadeOut('500');
		}, 2000);
		initialLoad = false;
	}


	/**
	 *
	 * Fixed Header
	 *
	 */

	var $wpAdminBar = $('#wpadminbar');
    $(window).scroll(function() {
		if ($wpAdminBar.length) {
	        $('.navbar-fixed-top').css('top', $wpAdminBar.height()+'px');
	    }

        var scroll = $(window).scrollTop();
        if (scroll > 450 && $(window).width() > 991) {
            $('.fixed-top').addClass('navbar-fixed-top');
        } else {
            $('.fixed-top').removeClass('navbar-fixed-top');
        }
    });

	/**
	 *
	 * Click Canvas Menu
	 *
	 */

	if ($wpAdminBar.length) {
		$('.jms-mobile-menu').css('top', $wpAdminBar.height()+'px');
	}

	var bodyEl = $('body'),
		content = $('#main-site'),
		openbtn = $('.menu-button'),
		closebtn = $('.close-button' ),
		isOpen = false;

	function init() {
		initEvents();
	}

	function initEvents() {
		openbtn.click(function(e) {
			toggleMenu();
			e.stopPropagation();
		});
		if( closebtn ) {
			closebtn.click(function() {
				toggleMenu();
			});
		}
		content.click(function(e) {
			var target = e.target;
			if( isOpen && target !== openbtn ) {
				toggleMenu();
			}
		});
	}

	function toggleMenu() {
		if( isOpen ) {
			bodyEl.removeClass('show-menu');
		}
		else {
			bodyEl.addClass('show-menu');
		}
		isOpen = !isOpen;
	}
	init();


	/**
	 *
	 * Search Button Header 4 - Responsive
	 *
	 */

	$('.header-4 .adv-search > a').click(function(e) {
		e.preventDefault();
		$('.header-4 .adv-search form').toggle('400');
	});

	/**
	 *
	 * Replace button
	 *
	 */

	$('.widget_wysija .wysija-submit').replaceWith('<button class="wysija-submit" type="submit">SUBSCRIBE</button>');

	// Back to top click
	if ($('.back-to-top').length) {
	    var scrollTrigger = 100, // px
	    backToTop = function () {
	        var scrollTop = $(window).scrollTop();
	        if (scrollTop > scrollTrigger) {
	            $('.back-to-top').addClass('show');
	        } else {
	            $('.back-to-top').removeClass('show');
	        }
	    };
	    backToTop();
	    $(window).on('scroll', function () {
	        backToTop();
	    });
	    $('.back-to-top').on('click', function (e) {
	        e.preventDefault();
	        $('html,body').animate({
	            scrollTop: 0
	        }, 700);
	    });
	}

	// Vertical Menu
	var menu_title = $('.vertical-wrap .title-block');
    var menuinner = $('.vertical-wrap .megamenu-inner');

    menu_title.click(function(e) {
		e.preventDefault();
		menuinner.toggle();
	});

	$('.jms-mobile-menu #menu-main-menu').children().click(function(e){
		$(this).children('.sub-menu').slideToggle('slow');
    //select all the `.child` elements and stop the propagation of click events on the elements
    }).children('.sub-menu').click(function (event) {
        event.stopPropagation();
    });

	$('.jms-mobile-menu #menu-main-menu > li > .sub-menu').children().click(function(e){
	    $(this).children('.sub-menu').slideToggle('slow');

	//select all the `.child` elements and stop the propagation of click events on the elements
	}).children('.sub-menu').click(function (event) {
	    event.stopPropagation();
	});

	//related-carousel
	var rtl = false;
	if ($('body').hasClass('rtl')) rtl = true;
	$('.related-carousel').owlCarousel({
		rtl: rtl,
		responsive : {
			320 : {
				items: 1,
			},
			768 : {
				items: 3,
			},
			991 : {
				items: 4,
			},
			1199 : {
				items: 4,
			}
		},
		margin: 30,
		dots: true,
		nav: false,
		autoplay: false,
		loop: false,
		autoplayTimeout: 5000,
		smartSpeed: 500
	});

	/**
	 *
	 * Ordering - Add Icon Dropdown
	 *
	 */
	$('.filters-panel .woocommerce-ordering').append('<span class="icon-down fa fa-caret-down"></span>');

	/**
	 *
	 * Product Grid or Product List
	 *
	 */
	var buttonGrid = $('.view-mode .view-grid'),
		buttonList = $('.view-mode .view-list'),
		productList = $('.products-list');

	buttonGrid.addClass('active');
	productList.addClass('product-list-column');

	buttonGrid.click(function(e) {
		e.preventDefault();
		$(this).addClass('active');
		buttonList.removeClass('active');
		productList.removeClass('product-list-row');
		productList.addClass('product-list-column');
	});

	buttonList.click(function(e) {
		e.preventDefault();
		$(this).addClass('active');
		buttonGrid.removeClass('active');
		productList.removeClass('product-list-column');
		productList.addClass('product-list-row');

	});

	// Add + sub quantity
	$('.quantity').on('click', '.add', function(e) {
        $input = $(this).prev('input.qty');
        var val = parseInt($input.val());
        $input.val( val+1 ).change();
    });

    $('.quantity').on('click', '.sub', function(e) {
        $input = $(this).next('input.qty');
        var val = parseInt($input.val());
        if (val > 1) {
            $input.val( val-1 ).change();
        }
    });



	//Mobile Menu
	var dropdownToggle = $( '<span />', {
			'class': 'dropdown-toggle'
	}).append( $( '<span />', {
			'class': 'fa fa-plus'
	} ) );
	$('.mobile-menu').find( '.menu-item-lv0.menu-item-has-children > a' ).append( dropdownToggle );

	$('.mobile-menu').find( '.dropdown-toggle' ).click( function( e ) {
		e.preventDefault();
		var _parent = $( this ).closest('.menu-item-lv0');
		_parent.toggleClass('open');

		if(_parent.hasClass('open')) {
			$( this ).find('.fa').removeClass('fa-plus');
			$( this ).find('.fa').addClass('fa-minus');
		} else {
			$( this ).find('.fa').removeClass('fa-minus');
			$( this ).find('.fa').addClass('fa-plus');
		}
	});


});
