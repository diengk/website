$(function() {
  $(".expand").on( "click", function() {
    // $(this).next().slideToggle(200);
    $expand = $(this).find(">:first-child");
    
    if($expand.text() == "+") {
    	$(this).css("background","#462976");
    	$(this).css("color","white");
    	$(this).find(".pull-right").css("color","white");
    	$(this).find(".pull-right").css("border-color","white");
      $expand.text("-");
    } else {
    	$(this).css("background","#ffffff");
    	$(this).css("color","#000");
    	$(this).find(".pull-right").css("color","#000");
    	$(this).find(".pull-right").css("border-color","#000");
      $expand.text("+");
    }
  });
});